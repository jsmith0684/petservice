# routes/users.rb
require_relative '../lib/helpers'
require_relative '../repositories/UserRepository'
require 'logger'

module API
  class Users < Roda
    plugin :json
    plugin :all_verbs
    plugin :default_headers, 'Content-Type' => 'application/json'
    plugin :halt
    plugin :json_parser
    plugin :error_handler do |e|
      { error: e.message }
    end

    log = Logger.new($stdout)
    route do |r|
      r.on do
        r.on Integer do |uid|
          log.debug("uid=#{uid}")
          @user = UserRepository.getUser(uid)

          r.halt(404) unless @user

          r.get 'pets' do
            { pets: UserRepository.getUserPets(@user) }
          end

          r.get 'findPets' do
            { pets: UserRepository.findUserPets(@user) }
          end

          r.get do
            { user: @user.to_api() }
          end

          r.put do |uid|
            r.halt(401) unless !r.params['token'].nil? && authorized?(r.params['token'])
            log.debug("params=#{r.params['role']}")
            res = UserRepository.updateUser(@user, user_params(r))
            if res.is_a?(Hash) && res[:error]
              response.status = 400
              return res
            else
              response.status = 204
              {}
            end
          end
        end

        r.get do
          { users: UserRepository.getUsers().map(&:to_api) }
        end

        r.post do
          log.debug("r.params=#{r.params}")
          users = UserRepository.createUsers(r.params['users'])
          if users.is_a?(Hash) && users[:error]
            response.status = 400
            return users
          else
            response.status = 201
            { users: users.map(&:to_api) }
          end
        end
      end
    end

    private

    def user_params(r)
      { profile: r.params['profile'],
        sex: r.params['sex'],
        min: r.params['min'],
        max: r.params['max']
      }.compact
    end
  end
end



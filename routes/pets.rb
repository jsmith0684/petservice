# routes/pets.rb
require_relative '../models/pet'
require_relative '../repositories/PetRepository'
require 'logger'

module API
  class Pets < Roda
    plugin :json
    plugin :all_verbs
    plugin :default_headers, 'Content-Type' => 'application/json'
    plugin :json_parser
    plugin :error_handler do |e|
      { error: e.message }
    end

    log = Logger.new($stdout)
    route do |r|
      r.on do
        r.get do
          { pets: PetRepository.getPets() }
        end

        r.post do
          log.debug("params=#{r.params['pets']}")
          pets = PetRepository.createPets(r.params['pets'])
          if pets.is_a?(Hash) && pets[:error]
            response.status = 400
            return pets
          else
            response.status = 201
            { pets: pets }
          end
        end
      end
    end
  end
end

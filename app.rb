# app.rb
require 'roda'
require 'json'
require 'yaml'
require 'sequel'
require_relative 'routes/pets'
require_relative 'routes/users'

module API

  class App < Roda
    plugin :json
    plugin :all_verbs
    plugin :default_headers, 'Content-Type' => 'application/json'
    plugin :multi_run
    plugin :error_handler do |e|
      { error: e.message }
    end

    route do |r|
      r.is 'v1' do
        r.get do
          { msg: 'ping' }
        end
      end
      r.on 'v1' do
        r.multi_run
      end
    end
  end
end

API::App.run("users", API::Users)
API::App.run("pets", API::Pets)


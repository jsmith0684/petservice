FROM alpine:3.10.1
MAINTAINER Joe Smith

ENV BUILD_PACKAGES bash curl-dev ruby-dev build-base sqlite sqlite-dev
ENV RUBY_PACKAGES ruby ruby-io-console ruby-bundler ruby-rake ruby-bigdecimal ruby-json
ENV GEM_HOME="/usr/local/bundle"
ENV PATH $GEM_HOME/bin:$GEM_HOME/gems/bin:$PATH

# Update and install all of the required packages.
# At the end, remove the apk cache
RUN apk update && \
    apk upgrade && \
    apk add $BUILD_PACKAGES && \
    apk add $RUBY_PACKAGES && \
    rm -rf /var/cache/apk/*

RUN mkdir /usr/app
WORKDIR /usr/app

COPY Gemfile /usr/app/
#COPY Gemfile.lock /usr/app/
RUN bundle install

COPY . /usr/app

RUN rake db:create && \
    rake db:migrate && \
    rake db:seed

EXPOSE 9292
ENTRYPOINT ["rackup"]


def valid_token?(t)
  @user.token == t
end

def engineer?(t)
  user ||= User.find(token: t)
  user.role == 'engineer'
end


def authorized?(token)
  if valid_token?(token) || engineer?(token)
    return true
  end
  false
end

# models/pet.rb
require_relative '../db/db'
class Pet < Sequel::Model

  many_to_many :users

  plugin :json_serializer
  plugin :validation_helpers

  def validate()
    validates_includes %w(m f), :sex
    validates_integer :age
    validates_includes  %w(cat dog), :type
  end
end

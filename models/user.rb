# models/user.rb
require 'securerandom'
require_relative '../db/db'

class User < Sequel::Model

  many_to_many :pets

  plugin :json_serializer
  plugin :validation_helpers

  def before_create
    assign_token()
    super()
  end

  def to_api()
    {
      id: id,
      profile: profile,
      typePreference: typePreference,
      ageRange: {
        min: min,
        max: max
      }
    }
  end

  def assign_token()
    self.token = generate_token
  end

  def generate_token()
    SecureRandom.hex(rand(15..25))
  end

  def validate()
    validates_includes ['m', 'f', nil], :sex
    validates_includes ['cat', 'dog', nil], :typePreference
    validates_includes %w(user engineer), :role
    #validates_integer :min
    #validates_integer :max
    #validates_operator :>, 0, :min
    #validates_operator :>, :min, :max
  end
end

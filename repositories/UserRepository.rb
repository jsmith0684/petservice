# repositories/user.rb
require_relative '../models/user'
require_relative '../models/pet'
require_relative 'PetRepository'

class UserRepository

  def self.getUsers()
    User.last(5)
  end

  def self.getUser(uid)
    User.find(id: uid)
  end

  def self.getUserPets(user)
    return user ? user.pets : nil
  end

  def self.findUserPets(user)
    if user
      min = user.min || 0
      max = user.max || 1000
      filters = {:age => min..max, :type => user.typePreference, :sex => user.sex}.compact
      PetRepository.getPets(filters)
    else
      return nil
    end
  end

  def self.createUsers(users)
    # multi_insert results in before_create hooks not being called in model
    #User.multi_insert(users)
    begin
      users.map { |u| User.create(u) }
    rescue ArgumentError, Sequel::ValidationFailed => e
      return { error: e.message }
    end
  end

  def self.updateUser(user, params)
    begin
      return user ? user.update(params) : nil
    rescue ArgumentError, Sequel::ValidationFailed => e
      return { error: e.message }
    end
  end
end

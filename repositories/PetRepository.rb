# repositories/PetRepository.rb
require_relative '../models/pet'

class PetRepository
  def self.getPets(filters={})
    Pet.where(filters)
  end

  def self.createPets(pets)
    begin
      pets.map { |p| Pet.create(p) }
    rescue ArgumentError, Sequel::ValidationFailed => e
      return { error: e.message }
    end
  end
end

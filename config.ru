require File.expand_path('app')
ENV['RACK_ENV'] = 'deployment'
run Rack::URLMap.new('/api' => API::App.app)

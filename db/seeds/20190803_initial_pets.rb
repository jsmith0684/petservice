require_relative '../../models/pet'
require 'literate_randomizer'
Sequel.seed do
  def run()
    animals = %w(dog cat)
    suffixes = %w(.com .net .org)
    sex = %w(m f)
    (1..21).each do |n|
      Pet.create(
        type: animals.sample(),
        name: LiterateRandomizer.word(),
        img: "http://#{LiterateRandomizer.word}#{suffixes.sample()}",
        age: rand(0..20),
        profile: LiterateRandomizer.sentence(),
        sex: sex.sample(),
      )
    end
  end
end

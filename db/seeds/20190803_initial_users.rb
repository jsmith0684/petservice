require_relative '../../models/user'
require 'literate_randomizer'
Sequel.seed do
  def run()
    roles = %w(user engineer)
    sexes = ['m', 'f', nil]
    types = ['cat', 'dog', nil]
    (1..21).each do |n|
      min = rand(0..20)
      max = rand(min..20)
      User.create(
        profile: LiterateRandomizer.sentence(),
        role: roles.sample(),
        sex: sexes.sample(),
        typePreference: types.sample(),
        min: min,
        max: max
      )
    end
  end
end

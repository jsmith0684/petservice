Sequel.seed do
  def run()
    i = 5
    (1..30).each do |n|
      begin
        DB[:pets_users].insert(pet_id: rand(1..21), user_id: rand(1..21))
      rescue SQLite3::ConstraintException, Sequel::UniqueConstraintViolation
        redo if (i -= 1) > 0
      end
    end
  end
end

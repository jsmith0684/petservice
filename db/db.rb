# db/db.rb
require 'sequel/core'

DB_FILE = 'db/db.sqlite'
DB = Sequel.connect("sqlite://#{DB_FILE}") unless defined?(DB)

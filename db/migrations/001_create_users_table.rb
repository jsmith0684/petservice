Sequel.migration do

  up do
    create_table(:users) do
      primary_key :id
      String :profile
      String :role, :element => ['user', 'engineer']
      String :sex, :element => ['m', 'f']
      String :typePreference, :element => ['dog', 'cat']
      Integer :min
      Integer :max
      String :token, null: false
    end
  end

  down do
    drop_table(:users)
  end
end


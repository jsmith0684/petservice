Sequel.migration do

  up do
    create_table(:pets) do
      primary_key :id
      String :type
      String :name
      String :img
      Integer :age
      String :profile
      enum :sex, :element => ['m', 'f']
    end
  end

  down do
    drop_table(:pets)
  end
end

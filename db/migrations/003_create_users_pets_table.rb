Sequel.migration do

  up do
    create_join_table(user_id: :users, pet_id: :pets)
  end

  down do
    drop_table(:pets_users)
  end
end

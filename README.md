## Pet Service

### Run the app
The suggested was for running the app is by building a Docker image and running the container. Directions for doing so are below.

• Download Docker from [Docker](https://docker.com)

• Install Docker

• Use Git to clone this repository: `git clone https://jsmith0684@bitbucket.org/jsmith0684/petservice.git`

• Navigate the newly clone petservice repository

• Build the docker image: `docker build -t cozi .`

• Run the newly built Docker image: `docker run -p 9292:9292 -it cozi:latest`

• The container will be accessible on localhost at port 9292

### Endpoints
GET /api/v1 - Get health check

GET /api/v1/users - Get the last 5 users

GET /api/v1/users/:id - Get a user matching the id

GET /api/v1/users/:id/pets - Get a list of user's saved pets

GET /api/v1/users/:id/findPets - Get a list of pets matching user's settings

GET /api/v1/pets - Get a list of the last 10 pets

POST /api/v1/users - Post a list of new users to be created

**Input**

```json
{  
   "users":[  
      {  
         "profile":"my profile",
         "typepreference":"cat",
         "min":12,
         "max":10,
         "role":"engineer"
      }
   ]
}
```
POST /api/v1/pets - Post a list of new pets to be created

**Input**

```json
{  
   "pets":[  
      {  
         "type":"dog",
         "sex":"f",
         "name":"lucy",
         "age":2,
         "profile":"young dog"
      },
      {  
         "type":"cat",
         "sex":"m",
         "name":"sylvester",
         "age":4,
         "profile":"sweet cat",
         "img":"http://google.com"
      }
   ]
}
```
PUT /api/v1/users/:id?token=:token - Update user matching id's properties. Properties that can be modified are profile, typepreference, min, and max. The token is appended to the endpoint as a query parameter. A user with role user can use his/her token to update his/her own properties. A user with role engineer can update any user's properties.

**Input**

```json
{
  "sex":"m"
}
```

### Tokens
A token is required when issuing a put request to the endpoint /api/v1/users/:id?token=:token. It should be appended to the path as a query parameter. Tokens are created when a new user is persisted to the database.

### TODOs
• Add unit tests

• Add pagination for getUsers and getPets API calls

• Refactor routes to extract handling functionality

• Add support for multiple environments

• Add configuration for puma application server

• Add actual authentication


	
